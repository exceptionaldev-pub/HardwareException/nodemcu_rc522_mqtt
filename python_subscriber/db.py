import mysql.connector as dba
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
from datetime import datetime


def insert(UID, position):
    try:
        mySQLConnection = mysql.connector.connect(host="localhost",
                                                  user="root",
                                                  passwd="milad",
                                                  database="rfid_db")
        cursor = mySQLConnection.cursor()
        sql_select_query = """select id_card from rfid_cards where card_uid = %s"""
        cursor.execute(sql_select_query, (UID, ))
        record = cursor.fetchall()
        print(UID)
        for row in record:
            print("Id = ", row[0], )
            if position == "device1/rfid":
                position = "ICU"
            if position == "device2/rfid":
                position = "CCU"
            if position == "device3/rfid":
                position = "Emergency"
            insertToDB(row[0], position)
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        # closing database connection.
        if (mySQLConnection.is_connected()):
            cursor.close()
            mySQLConnection.close()
            print("connection is closed")


def insertToDB(idCard, position):
    try:
        connection = mysql.connector.connect(host="localhost",
                                             user="root",
                                             passwd="milad",
                                             database="rfid_db")
        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO `sick_changes`(`id_card`, `position`) VALUES (%s,%s)"""
        insert_tuple = (idCard, position)
        result = cursor.execute(sql_insert_query, insert_tuple)
        connection.commit()
        print("Record inserted successfully into python_users table")
    except mysql.connector.Error as error:
        connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))
    finally:
        # closing database connection.
        if(connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")