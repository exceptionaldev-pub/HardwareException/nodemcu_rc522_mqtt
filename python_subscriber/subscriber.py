import paho.mqtt.client as pmqtt
import db
import time

def subscribe(host="localhost", port=1883, topic=[('device1/rfid', 0), ('device2/rfid', 0), ('device3/rfid', 0)], username="", password=""):
    if topic is None:
        topic = [('/default', 0)]
    client = pmqtt.Client()

    def on_connect(client, userdata, dict, rc):
        print('Connected with result code ' + str(rc))
        client.subscribe(topic)

    client.on_connect = on_connect

    def on_message(client, userdata, msg):
        print("__________________\n")
        print('Topic: ', msg.topic + '\nMessage: ' + str(msg.payload))
        print("__________________\n")
        db.insert((msg.payload).decode('utf8'), msg.topic)

    client.on_message = on_message

    client.connect(host, port)

    client.loop_forever()


subscribe()
